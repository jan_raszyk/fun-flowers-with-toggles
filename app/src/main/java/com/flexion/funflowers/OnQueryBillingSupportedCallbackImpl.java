package com.flexion.funflowers;

import com.flexionmobile.sdk.billing.BillingError;
import com.flexionmobile.sdk.billing.FlexionBillingService;
import com.flexionmobile.sdk.billing.ItemType;
import com.flexionmobile.sdk.billing.OnQueryBillingSupportedCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class OnQueryBillingSupportedCallbackImpl implements OnQueryBillingSupportedCallback {

    private static final Map<ItemType, List<String>> ITEM_IDS;

    static {
        ITEM_IDS = new HashMap<>();
        ITEM_IDS.put(ItemType.IN_APP, Arrays.asList(FlowerActivity.ITEM_ID_SEEDS, FlowerActivity.ITEM_ID_PREMIUM_THEME));
        ITEM_IDS.put(ItemType.SUBSCRIPTION, Arrays.asList(FlowerActivity.ITEM_ID_MAGICAl_WATER));
    }

    private final FlowerActivity flowerActivity;
    private final FlexionBillingService billingService;
    private final InitialiseCallback initialiseCallback;

    public OnQueryBillingSupportedCallbackImpl(FlowerActivity flowerActivity, FlexionBillingService billingService, InitialiseCallback initialiseCallback) {
        this.flowerActivity = flowerActivity;
        this.billingService = billingService;
        this.initialiseCallback = initialiseCallback;
    }

    @Override
    public void onSuccess(ItemType itemType) {
        flowerActivity.setBillingSupportedInApp(true);
        getPurchases(ItemType.IN_APP);
        getItemDetails(ItemType.IN_APP);
        checkIfSubscriptionIsSupported();
    }

    @Override
    public void onError(BillingError billingError) {
        flowerActivity.setBillingSupportedInApp(false);
        if (FunToggles.get().ignoreInAppBillingSupport()) {
            getPurchases(ItemType.IN_APP);
            getItemDetails(ItemType.IN_APP);
        }
        checkIfSubscriptionIsSupported();
    }

    private void checkIfSubscriptionIsSupported() {
        billingService.isBillingSupported(ItemType.SUBSCRIPTION, new OnSubscriptionBillingSupportedQueryCallback());
    }

    private class OnSubscriptionBillingSupportedQueryCallback implements  OnQueryBillingSupportedCallback {

        @Override
        public void onSuccess(ItemType itemType) {
            flowerActivity.setBillingSupportedSubs(true);
            getPurchases(ItemType.SUBSCRIPTION);
            getItemDetails(ItemType.SUBSCRIPTION);

            initialiseCallback.onComplete();
        }

        @Override
        public void onError(BillingError billingError) {
            flowerActivity.setBillingSupportedSubs(false);
            if (FunToggles.get().ignoreSubscriptionBillingSupport()) {
                getPurchases(ItemType.SUBSCRIPTION);
                getItemDetails(ItemType.SUBSCRIPTION);
            }
            initialiseCallback.onComplete();
        }

    }

    private void getPurchases(ItemType itemType) {
        billingService.getPurchases(itemType, ITEM_IDS.get(itemType), flowerActivity.getOnQueryGetPurchasesFinishedCallback());
    }

    private void getItemDetails(ItemType itemType) {
        List<String> itemIds;
        if (FunToggles.get().dirtyItemDetailRequest()) {
            itemIds = new ArrayList<>();
            itemIds.addAll(ITEM_IDS.get(ItemType.IN_APP));
            itemIds.addAll(ITEM_IDS.get(ItemType.SUBSCRIPTION));
        } else {
            itemIds = ITEM_IDS.get(itemType);
        }

        billingService.getItemDetails(itemType, itemIds, flowerActivity.getOnQueryItemDetailsFinishedCallback());
    }
}
