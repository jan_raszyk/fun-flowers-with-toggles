package com.flexion.funflowers;

public class DefaultFunToggles extends FunToggles {

    @Override
    public boolean ignoreSubscriptionBillingSupport() {
        return false;
    }

    @Override
    public boolean displayBillingSupportOnStartup() {
        return false;
    }

    @Override
    boolean ignoreInAppBillingSupport() {
        return false;
    }

    @Override
    boolean dirtyItemDetailRequest() {
        return false;
    }
}
