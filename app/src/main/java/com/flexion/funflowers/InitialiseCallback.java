package com.flexion.funflowers;

public interface InitialiseCallback {

    void onComplete();
}
