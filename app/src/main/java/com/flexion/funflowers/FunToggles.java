package com.flexion.funflowers;

public abstract class FunToggles {

    public static final FunToggles INSTANCE = new DefaultFunToggles();

    public static FunToggles get() {
        return INSTANCE;
    }

    /**
     * Game proceeds to make further IAP API calls even if isBillingSupported(subs) returns false
     */
    abstract boolean ignoreSubscriptionBillingSupport();

    /**
     * Game proceeds to make further IAP API calls even if isBillingSupported(inapp) returns false
     */
    abstract boolean ignoreInAppBillingSupport();

    /**
     * Game displays two dialogs on startup that show what return values were obtained from isBillingSupported(subs)
     * and from isBillingSupported(inapp).
     */
    abstract boolean displayBillingSupportOnStartup();

    /**
     * Ignore item type when making item detail requests and store returned data.
     */
    abstract boolean dirtyItemDetailRequest();
}
