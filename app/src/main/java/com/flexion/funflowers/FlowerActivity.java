/* 
 * TrivialDrive copyright 2012 Google Inc.
 * Fun Flowers copyright 2015 Flexion Mobile Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flexion.funflowers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.flexionmobile.sdk.Flexion;
import com.flexionmobile.sdk.billing.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
Fun Flowers<br><br>

Example game using the Flexion billing SDK. The app uses a local simulated Flexion
billing server, so it should work as a stand-alone application. <br><br>

This app is a simple game where the player can buy seeds and use it to 'grow'
randomly generated flowers. The player starts the game with a set amount of seeds. 
When the player grows a new flower, they consume a seed. If the player runs 
out of seeds, they can buy more using an in-app purchase.<br><br>

The user can also purchase a "premium upgrade" that unlocks a special theme
for the app.<br><br>

The user can also purchase a subscription ("magical water") which will 
make the flowers they grow larger. <br><br>

It's important to note the consumption mechanics for each item:<br><br>

PREMIUM THEME: the item is purchased and NEVER consumed. So, after the original
purchase, the player will always own that item. The application knows to
display the special picture because it queries whether the premium "item" is
owned or not.<br><br>

MAGICAL WATER: this is a subscription, and subscriptions can't be consumed.<br><br>

SEEDS: when seeds are purchased, the "seeds" item is then owned. We
consume it when we apply that item's effects to our app's world, which to
us means giving the player a fixed number of seeds. This happens immediately
after purchase! It's at this point (and not when the user grows a flower) that the
"seeds" item is CONSUMED. Consumption should always happen when your game
world was safely updated to apply the effect of the purchase. So, in an
example scenario:<br><br>

BEFORE:      the player has 5 seeds<br>
ON PURCHASE: the player has 5 seeds, "seeds" item is owned<br>
IMMEDIATELY: the player has 25 seeds, "seeds" item is consumed<br>
AFTER:       the player has 25 seeds, "seeds" item NOT owned any more<br><br>

Another important point to notice is that it may so happen that
the application crashed (or anything else happened) after the user
purchased the "seeds" item, but before it was consumed. That's why,
on startup, we check if we own the "seeds" item, and, if so,
we have to apply its effects to our world and consume it. This
is also very important!<br><br>

@author TrivalDrive originally by Bruno Oliveira (Google). Modified to
Fun Flowers by Jonathan Coe (Flexion). 
 */
public class FlowerActivity extends Activity {
	
    /** Does the user have the premium upgrade? */
    private boolean mIsPremium = false;

    /** Is the user currently subscribed to the 'magical water' upgrade? */
    private boolean mSubscribedToMagicalWater = false;

    /** Item ID for the premium theme item */
    static final String ITEM_ID_PREMIUM_THEME = "1023610";

    /** Item ID for the seeds item */
    static final String ITEM_ID_SEEDS = "1023608";

    /** Item ID for the magical water item */
    static final String ITEM_ID_MAGICAl_WATER = "1023609";
    
    /** The number of displayed seeds that a purchase of one "seeds" item corresponds to.
     e.g. If the player buys one "seeds" item then they will receive this many 'seeds'
     to grow flowers with */
    private static final int SEEDS_PER_PURCHASE = 20;
    
    /** The number of seeds that the player starts with when they first run the game */
    private static final int PLAYER_STARTING_SEEDS = 20;
    
    /** A key value used to reference a stored variable that records whether the player
     * has the premium upgrade */
    private static final String KEY_IS_PREMIUM = "key_is_premium";
    
    /** A key value used to reference a stored variable that records the player's
     * available number of seeds */
    private static final String KEY_PLAYER_SEEDS = "key_player_seeds";
    
    /** A key value used to reference a stored variable that records whether the
     * player is subscribed to the magical water upgrade */
    private static final String KEY_SUBSCRIBED_TO_MAGICAL_WATER = "key_subscribed_to_magical_water";
        
    /** Current number of seeds that the player has */
    private long mPlayerSeeds;
    
    /** An int value that references the resource ID for the current flower top to use */ 
    private int mCurrentFlowerTopId;
    
    /** An int value that references the resource ID for the current flower bottom to use */ 
    private int mCurrentFlowerBottomId;
    
    /** A boolean that records whether or not the player has grown a flower yet */
    private boolean mFlowerGrown;
    
    /** Defines the size of the enlarged layout area to be used when the user is subscribed to
     * the 'magical water' upgrade */
    static final int ENLARGED_FLOWER_LAYOUT_SIZE = 300;
    
    /** The in-app-billing helper object */
    private FlexionBillingServiceAsync billingService;

    private boolean billingSupportedInApp = false;
    private boolean billingSupportedSubs = false;

    private Map<String, Item> inAppItemDetails;
    private Map<String, Item> subsItemDetails;

	/** The tag used to mark log messages from this class */
    private static final String TAG = "FlowerActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate() called");

        setContentView(R.layout.activity_flower);

        setWaitScreen(true);
        inAppItemDetails = new HashMap<>();
        subsItemDetails = new HashMap<>();

        Log.d(TAG, "Creating IabHelper instance");

        initialise(new InitialiseCallback() {
            @Override
            public void onComplete() {
                updateUi();
                setWaitScreen(false);
            }
        });
    }

    private void initialise(final InitialiseCallback initialiseCallback) {

        billingService = Flexion.createBillingService(this);

        billingService.isBillingSupported(ItemType.IN_APP,
                new OnQueryBillingSupportedCallbackImpl(this, billingService, initialiseCallback));
    }

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.i(TAG, "onResume() called");

        try {
            // Load game data and update the UI to reflect it
            loadData();
            updateUi();
        }
        catch (Exception e) {
        	Log.e(TAG, "Exception occurred in FlowerActivity.onResume(). The exception stack trace was: "
                    + getStackTraceString(e));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        saveData();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();

        saveData();
        // Dispose of the billing helper instance
        Log.d(TAG, "Destroying helper.");
        billingService.dispose();
    }
    
    // User clicked the 'grow flower' button
	public void onGrowFlowerButtonClicked(View arg0) {
        Log.d(TAG, "Grow flower button clicked");
        
        // Check whether the player has any available seeds
        if (mPlayerSeeds <= 0)
        {
        	displayAlert("Oh no! You have run out of seeds! Buy some more so you can keep growing flowers!");
        	return;
        }
        
        // Pick new flower parts to be displayed
        int[] flowerPartIds = FlowerPicker.pickFlowerParts();
        mCurrentFlowerTopId = flowerPartIds[0];
        mCurrentFlowerBottomId = flowerPartIds[1];
        
        // Record that the player has grown a flower
        mFlowerGrown = true;
        
        // Take one seed from the player
        mPlayerSeeds --;
        
        saveData();
        updateUi();
        
        // Log the player's new balance
        Log.d(TAG, "The player now has " + mPlayerSeeds + " seeds");
    }
    
    // User clicked the "Buy Seeds" button
    public void onBuySeedsButtonClicked(View arg0) {
        Log.d(TAG, "Buy seeds button clicked.");

        if (!billingSupportedInApp) {
            complain("In App items are not supported on your device yet. Sorry!");
            return;
        }

        setWaitScreen(true);
        // launch the seeds purchase UI flow.
        // We will be notified of completion via onPurchaseFinishedCallback
        Log.d(TAG, "Launching purchase flow for seeds");

        String payload = "FunFlowersSeedsDeveloperPayload";

        Item item = inAppItemDetails.get(ITEM_ID_SEEDS);

        billingService.launchPurchaseFlow(this, item.getId(), item.getType(), payload, onPurchaseFinishedCallback);
    }

    // User clicked the "Upgrade to Premium" button.
    public void onBuyPremiumThemeButtonClicked(View arg0) {
        Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");

        if (!billingSupportedInApp && !FunToggles.get().ignoreInAppBillingSupport()) {
            complain("In App items are not supported on your device yet. Sorry!");
            return;
        }

        setWaitScreen(true);

        String payload = "FunFlowersPremiumThemeDeveloperPayload";

        Item item = inAppItemDetails.get(ITEM_ID_PREMIUM_THEME);

        billingService.launchPurchaseFlow(this, item.getId(), item.getType(), payload, onPurchaseFinishedCallback);
    }

    // "Subscribe to magical water" button clicked. Explain to user, then start purchase
    // flow for subscription.
    public void onBuyMagicalWaterButtonClicked(View arg0) {
        if (!billingSupportedSubs && !FunToggles.get().ignoreSubscriptionBillingSupport()) {
            complain("Subscriptions are not supported on your device yet. Sorry!");
            return;
        }

        setWaitScreen(true);

        String payload = "FunFlowersMagicalWaterDeveloperPayload";

        Log.d(TAG, "Launching purchase flow for magical water subscription.");

        Item item;
        if (FunToggles.get().dirtyItemDetailRequest()) {
            item = inAppItemDetails.get(ITEM_ID_MAGICAl_WATER);
        } else {
            item = subsItemDetails.get(ITEM_ID_MAGICAl_WATER);
        }

        billingService.launchPurchaseFlow(this, item.getId(), item.getType(), payload, onPurchaseFinishedCallback);
    }

    private OnPurchaseFinishedCallback onPurchaseFinishedCallback = new OnPurchaseFinishedCallback(){

        @Override
        public void onSuccess(Purchase purchase){
            handlePurchase(purchase);
            updateUi();
            setWaitScreen(false);
        }

        @Override
        public void onPending(Purchase purchase){
            displayAlert("The purchase status is pending");

            ItemType itemType = purchase.getItemType();
            List<String> itemIds = new ArrayList<>(1);
            itemIds.add(purchase.getItemId());

            billingService.getPurchases(itemType, itemIds, onQueryGetPurchasesFinishedCallback);

            updateUi();
            setWaitScreen(false);
        }

        @Override
        public void onUserCanceled(){
            displayAlert("The purchase was canceled");

            setWaitScreen(false);
        }

        @Override
        public void onError(BillingError billingError){
            displayAlert("Unfortunately your purchase could not be completed.\n\n"
                    + "Message: " + billingError.getDescription());
            setWaitScreen(false);
        }
    };

    private OnQueryGetPurchasesFinishedCallback onQueryGetPurchasesFinishedCallback = new OnQueryGetPurchasesFinishedCallback() {
        @Override
        public void onSuccess(ItemType itemType, Map<String, Purchase> map) {
            for (Purchase purchase : map.values()) {
                handlePurchase(purchase);
            }
        }

        @Override
        public void onError(BillingError billingError) {
            displayAlert("Failed to get purchases.\n\n"
                    + "Message: " + billingError.getDescription());
        }
    };

    private void handlePurchase(Purchase purchase) {
        if (validatePurchase(purchase)) {
            if (purchase.getItemId().equals(ITEM_ID_SEEDS)) {
                // The user purchased one unit of seeds. Consume it.
                Log.d(TAG, "Purchase is seeds. Starting seed consumption.");
                billingService.consumePurchase(purchase.getToken(), onConsumeFinishedCallback);
            } else if (purchase.getItemId().equals(ITEM_ID_PREMIUM_THEME)) {
                // The user purchased the premium upgrade!
                Log.d(TAG, "Purchase is premium theme. Congratulating user.");
                displayAlert("Thank you for upgrading to premium!");
                mIsPremium = true;
            } else if (purchase.getItemId().equals(ITEM_ID_MAGICAl_WATER)) {
                // The user purchased the magical water subscription
                Log.d(TAG, "Magical water subscription purchased.");
                displayAlert("Thank you for subscribing to the magical water upgrade!");
                mSubscribedToMagicalWater = true;
            }
        }
    }

    private boolean validatePurchase(Purchase purchase) {
        try {
            boolean isValid = PurchaseValidator.verifyPurchaseData(purchase.toJson(), purchase.getSignature());
            if (isValid) {
                displayToast("Successful purchase validation", Toast.LENGTH_LONG);
                return true;
            } else {
                complain("Invalid purchase");
            }
        } catch (Exception e) {
            Log.d(TAG, "Purchase validation failed", e);
            complain("Purchase validation failed: " + e.getMessage() + "\nCheck device logs for details");
        }
        return false;
    }

    private OnQueryItemDetailsFinishedCallback onQueryItemDetailsFinishedCallback = new OnQueryItemDetailsFinishedCallback() {

        @Override
        public void onError(BillingError billingError) {

        }

        @Override
        public void onSuccess(ItemType itemType, Map<String, Item> map) {
            if (ItemType.IN_APP.equals(itemType) || FunToggles.get().dirtyItemDetailRequest()) {
                inAppItemDetails.putAll(map);
            } else {
                subsItemDetails = map;
            }

            updateUi();
        }
    };

    // Called when an attempt to consume an item has finished
    private OnConsumeFinishedCallback onConsumeFinishedCallback = new OnConsumeFinishedCallback() {

        @Override
        public void onError(BillingError billingError) {
            complain("Error while consuming: " + billingError.getDescription());
        }

        @Override
        public void onSuccess() {
            // We know this is the "seeds" item because it's the only one we consume,
            // so we don't check which item was consumed. If you have more than one
            // consumable item, you could consider implementing additional callbacks...

            mPlayerSeeds = mPlayerSeeds + SEEDS_PER_PURCHASE;
            saveData();
            displayAlert("You purchased " + SEEDS_PER_PURCHASE + " seeds!\n\n"
                    + "You now have " + mPlayerSeeds + " seeds to grow flowers with!");
            updateUi();
            Log.d(TAG, "Consumption successful");
        }
    };

    // Updates the UI to reflect the model
    private void updateUi() {
        UpdateUISupport.updateUi(this);
    }

    /** Enables or disables the "please wait" screen. */
    private void setWaitScreen(final boolean set) {
    	runOnUiThread(new Runnable() {
    	    public void run() {
		        findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
		        findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    	    }
    	});
    }
    
    /** 
     * Takes an error messages and:<br>
     * 	i)  Logs it<br>
     * 	ii) Displays an alert dialog with the message to the user 
     */
    private void complain(final String message) {
    	runOnUiThread(new Runnable() {
    	    public void run() {
		        Log.e(TAG, "**** Fun Flowers Error: " + message);
		        displayAlert("Error: " + message);
    	    }
    	});
    }
    
    /**
     * Display an alert message to the user
     * 
     * @param message - The alert message to display
     */
    private void displayAlert(final String message) {
    	runOnUiThread(new Runnable() {
    	    public void run() {
		        AlertDialog.Builder builder = new AlertDialog.Builder(FlowerActivity.this);
		        builder.setMessage(message);
		        builder.setNeutralButton("OK", null);
		        Log.d(TAG, "Showing alert dialog: " + message);
		        builder.create().show();
    	    }
    	});
    }

    /**
     * Display a toast message to the user
     *
     * @param message - The toast message to display
     */
    private void displayToast(final String message, final int duration) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(FlowerActivity.this, message, duration).show();
            }
        });
    }

    /**
     * Saves the player's game state. 
     */
    private void saveData() {
        /*
         * Note: in a real application, we recommend you save data in a secure way to
         * prevent tampering. For simplicity in this sample, we simply store the player data
         * in SharedPreferences.
         */
        SharedPreferences.Editor sharedPrefs = getPreferences(MODE_PRIVATE).edit();
        sharedPrefs.putBoolean(KEY_IS_PREMIUM, mIsPremium);
        sharedPrefs.putLong(KEY_PLAYER_SEEDS, mPlayerSeeds);
        sharedPrefs.putBoolean(KEY_SUBSCRIBED_TO_MAGICAL_WATER, mSubscribedToMagicalWater);
        sharedPrefs.commit();
        Log.i(TAG, "Saved player game data:\n"
        		+ "Player seeds: " + mPlayerSeeds + "\n"
        		+ "Player is premium: " + mIsPremium + "\n"
        		+ "Player subscribed to magical water: " + mSubscribedToMagicalWater);
    }
    
    /**
     * Loads the player's game state. 
     */
    private void loadData() {
        SharedPreferences sharedPrefs = getPreferences(MODE_PRIVATE);
        mIsPremium = sharedPrefs.getBoolean(KEY_IS_PREMIUM, false);
        mPlayerSeeds = sharedPrefs.getLong(KEY_PLAYER_SEEDS, PLAYER_STARTING_SEEDS);
        mSubscribedToMagicalWater = sharedPrefs.getBoolean(KEY_SUBSCRIBED_TO_MAGICAL_WATER, false);
        Log.i(TAG, "Loaded player game data:\n"
        		+ "Player seeds: " + mPlayerSeeds + "\n"
        		+ "Player is premium: " + mIsPremium + "\n"
        		+ "Player subscribed to magical water: " + mSubscribedToMagicalWater);
    }

    private String getStackTraceString(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    OnQueryItemDetailsFinishedCallback getOnQueryItemDetailsFinishedCallback() {
        return onQueryItemDetailsFinishedCallback;
    }

    OnQueryGetPurchasesFinishedCallback getOnQueryGetPurchasesFinishedCallback() {
        return onQueryGetPurchasesFinishedCallback;
    }

    boolean isPremium() {
        return mIsPremium;
    }

    boolean isSubscribedToMagicalWater() {
        return mSubscribedToMagicalWater;
    }

    boolean isFlowerGrown() {
        return mFlowerGrown;
    }

    int getCurrentFlowerTopId() {
        return mCurrentFlowerTopId;
    }

    int getCurrentFlowerBottomId() {
        return mCurrentFlowerBottomId;
    }

    long getPlayerSeeds() {
        return mPlayerSeeds;
    }

    Map<String, Item> getInAppItemDetails() {
        return inAppItemDetails;
    }

    Map<String, Item> getSubsItemDetails() {
        return subsItemDetails;
    }

    void setBillingSupportedInApp(boolean billingSupportedInApp) {
        this.billingSupportedInApp = billingSupportedInApp;
        if (FunToggles.get().displayBillingSupportOnStartup()) {
            displayAlert("In-app billing supported: " + (billingSupportedInApp ? "yes" : "no"));
        }
    }

    void setBillingSupportedSubs(boolean billingSupportedSubs) {
        this.billingSupportedSubs = billingSupportedSubs;
        if (FunToggles.get().displayBillingSupportOnStartup()) {
            displayAlert("Subscription billing supported: " + (billingSupportedSubs ? "yes" : "no"));
        }
    }
}