package com.flexion.funflowers;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flexionmobile.sdk.billing.Item;

class UpdateUISupport {

    private final FlowerActivity activity;

    static void updateUi(FlowerActivity activity) {
        final UpdateUISupport updateUISupport = new UpdateUISupport(activity);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                updateUISupport.updateUiPremiumDependent();
                updateUISupport.updateUiSubscribedToMagicalWaterDependent();
                updateUISupport.displaySeedsButton();
                updateUISupport.displayAvailableSeeds();
                updateUISupport.updateUiFlowerGrownDependent();
            }
        });
    }

    private UpdateUISupport(FlowerActivity flowerActivity) {
        this.activity = flowerActivity;
    }

    private void displayAvailableSeeds() {
        TextView playerSeedsTextView = (TextView) activity.findViewById(R.id.player_seeds);
        playerSeedsTextView.setText("Seeds: " + String.valueOf(activity.getPlayerSeeds()));
    }

    private void displaySeedsButton() {
        TextView seedsButtonTextView = (TextView) activity.findViewById(R.id.buy_seeds_button_textview);
        String baseText = activity.getResources().getString(R.string.buy_seeds_button_base_text);
        Item item = activity.getInAppItemDetails().get(FlowerActivity.ITEM_ID_SEEDS);
        if (item != null) {
            String seedsPrice = item.getPrice();
            seedsButtonTextView.setText(baseText + "\n" + seedsPrice);
        }
        else {
            seedsButtonTextView.setText(baseText);
        }
    }

    private void updateUiFlowerGrownDependent() {
        if (activity.isFlowerGrown()) {
            // Update the displayed flower components (currently top and bottom)
            ImageView flowerTop = ((ImageView) activity.findViewById(R.id.flower_top));
            ImageView flowerBottom = ((ImageView) activity.findViewById(R.id.flower_bottom));
            flowerTop.setImageResource(activity.getCurrentFlowerTopId());
            flowerBottom.setImageResource(activity.getCurrentFlowerBottomId());

            // If the user is subscribed to the 'magical water' upgrade, enlarge the flowers displayed
            if (activity.isSubscribedToMagicalWater()) {
                LinearLayout.LayoutParams layoutParams =
                        new LinearLayout.LayoutParams(FlowerActivity.ENLARGED_FLOWER_LAYOUT_SIZE, FlowerActivity.ENLARGED_FLOWER_LAYOUT_SIZE);
                flowerTop.setLayoutParams(layoutParams);
                flowerBottom.setLayoutParams(layoutParams);
            }
        }
    }

    private void updateUiSubscribedToMagicalWaterDependent() {
        if (activity.isSubscribedToMagicalWater()) {
            activity.findViewById(R.id.buy_magical_water_button_layout).setVisibility(View.GONE);
        }
        else {
            activity.findViewById(R.id.buy_magical_water_button_layout).setVisibility(View.VISIBLE);
            TextView magicalWaterButtonTextView = (TextView) activity.findViewById(R.id.buy_magical_water_button_textview);
            String baseText = activity.getResources().getString(R.string.buy_magical_water_button_base_text);

            // If the item price for 'Magical Water' is available, display it
            Item item;
            if (FunToggles.get().dirtyItemDetailRequest()) {
                item = activity.getInAppItemDetails().get(FlowerActivity.ITEM_ID_MAGICAl_WATER);
            } else {
                item = activity.getSubsItemDetails().get(FlowerActivity.ITEM_ID_MAGICAl_WATER);
            }
            if (item != null) {
                String magicalWaterPrice = item.getPrice();
                magicalWaterButtonTextView.setText(baseText + "\n" + magicalWaterPrice);
            }
            else {
                magicalWaterButtonTextView.setText(baseText);
            }
        }
    }

    private void updateUiPremiumDependent() {
        if (activity.isPremium()) {
            ((ImageView) activity.findViewById(R.id.free_or_premium)).setImageResource(R.drawable.premium_theme);
            activity.findViewById(R.id.title).setVisibility(View.GONE);
            activity.findViewById(R.id.buy_premium_theme_button_layout).setVisibility(View.GONE);
        }
        else {
            ((ImageView) activity.findViewById(R.id.free_or_premium)).setImageResource(R.drawable.free);
            activity.findViewById(R.id.buy_premium_theme_button_layout).setVisibility(View.VISIBLE);
            TextView premiumThemeButtonTextView = (TextView) activity.findViewById(R.id.buy_premium_theme_button_textview);
            String baseText = activity.getResources().getString(R.string.buy_premium_theme_button_base_text);

            // If the item price for the 'Premium Theme' is available, display it
            Item item = activity.getInAppItemDetails().get(FlowerActivity.ITEM_ID_PREMIUM_THEME);
            if (item != null) {
                String premiumThemePrice = item.getPrice();
                premiumThemeButtonTextView.setText(baseText + "\n" + premiumThemePrice);
            }
            else {
                premiumThemeButtonTextView.setText(baseText);
            }
        }
    }
}
