Building Fun Flowers
=====================

Overview
------
Fun Flowers is currently developed and built using Gradle: [https://gradle.org/](https://gradle.org/).
We recommend Android Studio or IntelliJ IDEA to manage and build this project.


Setting up a Development Environment
------

1) Make sure you have a Java JDK installed. The Android team recommends the Oracle JDK is over the Open JDK.

2) Download the Android Studio: [https://developer.android.com/sdk/index.html#download](https://developer.android.com/sdk/index.html#download)

3) If it is not already installed, install [git](http://git-scm.com)

4) Clone the Fun Flowers repository to your system: 

```sh
git clone https://bitbucket.org/flexionmobile-ondemand/fun-flowers.git
```

5) Import Fun Flowers into Android Studio

6) If you are on 64 bit Linux you may encounter build errors at this stage. You can try to fix these by installing some extra packages: sudo apt-get install lib32stdc++6 lib32z1 lib32ncurses5 lib32bz2-1.0


Building APK With Gradle
-------

1) Get Gradle 2.14.1

2) Get JDK 1.8, set JAVA_HOME to point to JDK 1.8

3) Get git

4) Clone Fun Flowers repository

5) Run "gradle assemble" in Fun Flowers root

6) Resulting APK file is in ./app/build/outputs/apk/
